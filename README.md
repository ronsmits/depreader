Load `dependencies.json` into a simple sqlite database

## Reason
when working with old and sometimes not very well maintained large projects, dependencies management can be quite hard. There are tools that show dependencies for a project, but 
not overarching projects. Also it makes it hard to see whAt dependency uplifted a transient dependency.

By being able to load dependencies into a database so you can query and group dependencies from several projects at the same time.

## Usage
1. compile this project into a self executing fat jar: `gradlew shadowJar`
1. add the jsonDependencyReport to `build.gradle`. See [plugin page](https://plugins.gradle.org/plugin/io.github.kota65535.dependency-report) for more information. Ensure you use the correct version depending on the gradle version.
1. run `java -jar build/libs/depreader-1.0-SNAPSHOT-all.jar build/reports/project/dependencies.json` (adjust the path as needed). This will load the database.
1. use an sql explorer (like from Intellij) to query and examine the data.

## Example

| groupId              | artifactId           | version        | project    | parent dependency                                |
|----------------------|----------------------|----------------|------------|--------------------------------------------------|
| org.jetbrains.kotlin | kotlin-stdlib        | 1.9.23         | depreader, |                                                  |
| org.jetbrains        | annotations          | 13.0           | depreader  | org.jetbrains.kotlin:kotlin-stdlib:1.9.23        |
| org.jetbrains.kotlin | kotlin-stdlib-jdk8   | 1.8.0          | depreader  | org.jetbrains.kotlin:kotlin-stdlib:1.9.23        |
| org.jetbrains.kotlin | kotlin-stdlib        | 1.8.0 ➡ 1.9.23 | depreader  | org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.0    |
| org.jetbrains.kotlin | kotlin-stdlib-jdk7   | 1.8.0          | depreader  | org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.8.0    |
| org.jetbrains.kotlin | kotlin-stdlib        | 1.8.0 ➡ 1.9.23 | depreader  | org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.8.0    |
| org.jetbrains.kotlin | kotlin-stdlib-jdk7   | 1.8.0          | depreader  | org.jetbrains.kotlin:kotlin-stdlib:1.9.23        |
| org.jetbrains.kotlin | kotlin-stdlib-common | 1.9.23         | depreader  | org.jetbrains.kotlin:kotlin-stdlib:1.9.23        |
| org.jetbrains.kotlin | kotlin-stdlib        | 1.9.23         | depreader  | org.jetbrains.kotlin:kotlin-stdlib-common:1.9.23 |
