import com.google.gson.JsonObject
import com.google.gson.JsonParser.parseString
import org.example.readFile
import kotlin.test.Test
import kotlin.test.assertEquals

class ReadFileTests {

    @Test
    fun readFileTest(): Unit {
        val s = readFile("src/test/resources/dependencies.json")
        val rootNode = parseString(s).asJsonObject
        val projName = rootNode.getAsJsonObject("project").get("name").asString
        assertEquals(projName, "depreader")
        rootNode
            .getAsJsonObject("project")
            .getAsJsonArray("configurations")
            .asSequence()
            .filterIsInstance(JsonObject::class.java)
            .filter { it.get("name").asString == "compileClasspath"}
            .first().getAsJsonArray("dependencies")
            .asSequence().filterIsInstance(JsonObject::class.java).forEach { println(it.get("name").asString) }
    }
}
