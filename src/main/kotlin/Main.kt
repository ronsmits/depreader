package org.example

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser.parseString
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File

fun main(args: Array<String>) {
    lateinit var projects:JsonArray

    val filename:String = if (args.size != 1) {
        System.err.println(
            "filename must be given\n" +
                    "this is usually called 'dependencies.json' look for it in 'build/reports/project' of the gradle project"
        )
        "dependencies.json"
    } else
        args[0]

    initDb()
    val rootNode = parseString(readFile(filename)).asJsonObject
    val projKey = rootNode.getAsJsonObject("project")
    if(projKey==null){
        projects = rootNode.getAsJsonArray("projects")
    } else {
        projects.set(0,projKey)
    }

    for (project in projects) {
        val projName = project.asJsonObject.get("name").asString
        println(projName)
        val configurations = project.asJsonObject.getAsJsonArray("configurations")
        val dependencies= configurations.asSequence().filterIsInstance(JsonObject::class.java)
            .filter{ it.get("name").asString == "compileClasspath"}
            .firstOrNull()?.getAsJsonArray("dependencies")
        if (dependencies!=null && !dependencies.isEmpty)
            dependencies.asSequence().filterIsInstance<JsonObject>().forEach {
                println("$projName - ${it.get("name")}")
                handleDependency(projName, it, "")
        }
    }
}

fun handleDependency(projName: String, dependency: JsonObject, index: String, parent: String = "") {
    val list = dependency.getAsJsonArray("children").toList()
    val (groupId, artifactId, version) = handleVersion( dependency.get("name").asString)
    println("${index}$projName - $groupId - $artifactId - $version")
    DependencyRepo.save(
        Dependency(
            group = groupId,
            artifact = artifactId,
            version = version,
            projectName = projName,
            parent = parent
        )
    )
    list.forEach { handleDependency(
        projName,
        it.asJsonObject,
        "$index  ",
        "$groupId:$artifactId:$version") }
}

fun handleVersion(versionStr:String): List<String> {
    val split :MutableList<String> = versionStr.split(":") as MutableList<String>
    if(split.size==2){
        split.add(2, "no version!")
    }
    return split
}
fun readFile(filePath: String) = File(filePath).readText()


internal fun initDb() {
    Database.connect(
        url = "jdbc:sqlite:sample.db",
        driver = "org.sqlite.JDBC",
        user = "conf",
        password = "conf"
    )
    transaction {
        SchemaUtils.create(DependencyTable)
    }
}