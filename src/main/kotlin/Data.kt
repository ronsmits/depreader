package org.example

import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

data class Dependency(
    val group: String,
    val artifact: String,
    val version: String,
    val projectName: String,
    val parent: String = ""
)

object DependencyTable : Table("dependency") {
    val group = varchar("group", 255)
    val artifact = varchar("artifact", 255)
    val version = varchar("version", 255)
    val projectName = varchar("project_name", 255)
    val parent = varchar("parent", 255)
}

object DependencyRepo {
    fun findAll() = transaction { DependencyTable.selectAll().map(ResultRow::toDependency) }

    fun findDependency(group: String, artifact: String, version: String, projectName: String): List<Dependency> =
        transaction {
            DependencyTable.select {
                (DependencyTable.group eq group) and
                        (DependencyTable.artifact eq artifact) and
                        (DependencyTable.version eq version) and
                        (DependencyTable.projectName eq projectName)
            }
        }.map { it.toDependency() }.toList()

    fun save(dependency: Dependency) = transaction {
        DependencyTable.insert {
            it[group] = dependency.group
            it[artifact] = dependency.artifact
            it[version] = dependency.version
            it[projectName] = dependency.projectName
            it[parent] = dependency.parent
        }
    }
}

private fun ResultRow.toDependency() = Dependency(
    group = this[DependencyTable.group],
    artifact = this[DependencyTable.artifact],
    version = this[DependencyTable.version],
    projectName = this[DependencyTable.projectName],
    parent = this[DependencyTable.parent]
)