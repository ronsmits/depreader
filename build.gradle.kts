plugins {
    kotlin("jvm") version "1.9.23"
    id("io.github.kota65535.dependency-report") version "2.0.1"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

group = "org.example"
version = "1.0-SNAPSHOT"
val exposedVersion = "0.41.1"
val gsonVersion = "2.11.0"
val derbyVersion = "10.15.2.0"
val sqliteVersion = "3.46.0.0"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("com.google.code.gson:gson:$gsonVersion")
    implementation ("org.jetbrains.exposed:exposed-core:$exposedVersion")
    implementation ("org.jetbrains.exposed:exposed-jdbc:$exposedVersion")
    implementation("org.xerial:sqlite-jdbc:$sqliteVersion")
    testImplementation(kotlin("test"))
}

tasks.test {
    useJUnitPlatform()
}
tasks.jar {
    manifest.attributes["Main-Class"] = "org.example.MainKt"
}
kotlin {
    jvmToolchain(17)
}